﻿using GTA;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace DOSIV
{
    public class DOSIV : Script
    {
        private const string sUseHoldKey = "use_hold_key", sHoldKey = "hold_key", sPressKey = "press_key", sKeys = "keys";
        private const bool defaultUseHoldKey = true;
        private const Keys defaultHoldKey = Keys.RControlKey, defaultPressKey = Keys.C;

        private bool useHoldKey;
        private Keys holdKey, pressKey;

        private bool dosWindowOpen = false, justClosed = false;
        private string dosCurrentCommand = "";
        private List<string> dosMessages = new List<string>();

        private Size resolution;
        private Point pos;
        private Size size;
        //private GTA.Font font;

        private Random mainRand;
        private Random yearRand;
        private Random ageRand;

        public DOSIV()
        {
            if (!File.Exists(Settings.Filename))
            {
                Settings.SetValue(sUseHoldKey, sKeys, defaultUseHoldKey);
                Settings.SetValue(sHoldKey, sKeys, defaultHoldKey);
                Settings.SetValue(sPressKey, sKeys, defaultPressKey);
                Settings.Save();
            }

            useHoldKey = Settings.GetValueBool(sUseHoldKey, sKeys, defaultUseHoldKey);
            holdKey = Settings.GetValueKey(sHoldKey, sKeys, defaultHoldKey);
            pressKey = Settings.GetValueKey(sPressKey, sKeys, defaultPressKey);

            resolution = Game.Resolution;
            pos = new Point(resolution.Width / 2, resolution.Height / 4);
            size = new Size((int)(resolution.Width / 1.010526315789474), (int)(resolution.Height / 3.764705882352941)); //1900x - 510y //archive: size = new Size(resolution.Width - resolution.Width / 96, resolution.Height - (int)(resolution.Height / 1.894736842105263f))
            //font = new GTA.Font(new System.Drawing.Font("Arial", 0.5f));

            mainRand = new Random();
            yearRand = new Random(mainRand.Next(999));
            ageRand = new Random(yearRand.Next(1000, 2000));

            KeyDown += DOSIV_KeyDown;
            PerFrameDrawing += DOSIV_PerFrameDrawing;
            Interval = 50;
            Tick += DOSIV_Tick;
        }

        private void DOSIV_Tick(object sender, EventArgs e)
        {
            if (dosWindowOpen)
            {
                Player.CanControlCharacter = false;
            }
            else if (justClosed)
            {
                Player.CanControlCharacter = true;
                justClosed = false;
            }
        }

        private TimeSpan timeSinceChanged = DateTime.Now.TimeOfDay;
        private string frameString = "";
        private void DOSIV_PerFrameDrawing(object sender, GraphicsEventArgs e)
        {
            if (dosWindowOpen)
            {

                e.Graphics.Scaling = FontScaling.Pixel;
                e.Graphics.DrawRectangle(960, 270, 1900, 510, Color.Black); //1920=960posx, 1920=1900wid, 1080=270posy, 1080=510hei
                float y = 30;
                if (dosMessages.Count > 0)
                {
                    for (int i = ((dosMessages.Count - 15 > 0) ? dosMessages.Count - 15 : 0); i < dosMessages.Count; i++)
                    {
                        e.Graphics.DrawText(dosMessages.ToArray()[i], 30, y, Color.White);
                        y += 30;
                    }
                }

                if (Math.Abs(DateTime.Now.TimeOfDay.TotalMilliseconds - timeSinceChanged.TotalMilliseconds) >= 500)
                {
                    frameString = frameString == "" ? "_" : "";
                    timeSinceChanged = DateTime.Now.TimeOfDay;
                }

                e.Graphics.DrawText("   >> " + dosCurrentCommand + frameString, 30, y, Color.White);
            }
        }

        private void DOSIV_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            KeyEvents(e);
        }

        private void KeyEvents(GTA.KeyEventArgs e)
        {
            if ((!useHoldKey || isKeyPressed(holdKey)) && e.Key == pressKey)
            {
                dosWindowOpen = !dosWindowOpen;
                if (!dosWindowOpen) justClosed = true;
            }
            else if (dosWindowOpen)
            {
                if (e.Key == Keys.Enter)
                {
                    dosMessages.Add("> " + dosCurrentCommand);
                    if (!string.IsNullOrWhiteSpace(dosCurrentCommand)) ProcessCommand(dosCurrentCommand);
                    dosCurrentCommand = "";
                }
                else if (e.Key == Keys.Back)
                {
                    if (dosCurrentCommand.Length > 0) dosCurrentCommand = dosCurrentCommand.Remove(dosCurrentCommand.Length - 1);
                }
                else if (MeasureStringWidth(dosCurrentCommand, new System.Drawing.Font(new FontFamily("Arial"), 0.5f)) < 1900 / 38) //== 50
                {
                    int iKey = (int)e.Key;
                    if (iKey >= 65 && iKey <= 90)
                    {
                        dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? e.Key.ToString() : e.Key.ToString().ToLower();
                    }
                    switch (e.Key)
                    {
                        case Keys.Space:
                            dosCurrentCommand += " ";
                            break;
                        case Keys.D0:
                        case Keys.NumPad0:
                            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? ")" : "0";
                            break;
                        case Keys.D1:
                        case Keys.NumPad1:
                            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "!" : "1";
                            break;
                        case Keys.D2:
                        case Keys.NumPad2:
                            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "@" : "2";
                            break;
                        case Keys.D3:
                        case Keys.NumPad3:
                            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "#" : "3";
                            break;
                        case Keys.D4:
                        case Keys.NumPad4:
                            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "$" : "4";
                            break;
                        case Keys.D5:
                        case Keys.NumPad5:
                            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "%" : "5";
                            break;
                        case Keys.D6:
                        case Keys.NumPad6:
                            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "^" : "6";
                            break;
                        case Keys.D7:
                        case Keys.NumPad7:
                            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "&" : "7";
                            break;
                        case Keys.D8:
                        case Keys.NumPad8:
                            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "*" : "8";
                            break;
                        case Keys.D9:
                        case Keys.NumPad9:
                            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "(" : "9";
                            break;
                        case Keys.OemPeriod:
                            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? ">" : ".";
                            break;
                        case Keys.OemOpenBrackets:
                            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "{" : "[";
                            break;
                        case Keys.OemCloseBrackets:
                            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "}" : "]";
                            break;
                        case Keys.Oemcomma:
                            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "<" : ",";
                            break;
                        case Keys.OemQuestion:
                            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "?" : "/";
                            break;
                        case Keys.OemBackslash:
                            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "|" : @"\";
                            break;
                        case Keys.OemSemicolon:
                            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? ":" : ";";
                            break;
                        case Keys.OemQuotes:
                            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "\"" : "'"; // \" == "
                            break;
                        case Keys.Oemplus:
                            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "+" : "=";
                            break;
                        case Keys.OemMinus:
                            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "_" : "-";
                            break;
                    }
                }
            }
        }

        /*
         * find help item by string
         * Player.Character.Task.CruiseWithVehicle
         * Player.Character.Task.DriveTo
         * Player.Character.Task.GoTo
         * Player.Character.Task.LandHelicopter
         * Player.Character.Task.LookAt
         * Player.Character.Task.RunTo
         * Player.Character.Task.TurnTo
         * Player.Character.Task.WanderAround
         * Player.Character.CurrentVehicle.AllowSirenWithoutDriver
         * Player.Character.CurrentVehicle.ApplyForce
         * Player.Character.CurrentVehicle.ApplyForceRelative
         * Player.Character.CurrentVehicle.BurstTire
         * Player.Character.CurrentVehicle.CanBeDamaged
         * Player.Character.CurrentVehicle.CanTiresBurst
         * Player.Character.CurrentVehicle.CloseAllDoors
         * Player.Character.CurrentVehicle.Color
         * Player.Character.CurrentVehicle.Direction
         * Player.Character.CurrentVehicle.Dirtyness
         * Player.Character.CurrentVehicle.DoorLock
         * Player.Character.CurrentVehicle.EngineHealth
         * Player.Character.CurrentVehicle.Explode
         * Player.Character.CurrentVehicle.FeatureColor1
         * Player.Character.CurrentVehicle.FeatureColor2
         * Player.Character.CurrentVehicle.FixTire
         * Player.Character.CurrentVehicle.FreezePosition
         * Player.Character.CurrentVehicle.HazardLightsOn
         * Player.Character.CurrentVehicle.Heading
         * Player.Character.CurrentVehicle.Health
         * Player.Character.CurrentVehicle.InteriorLightOn
         * Player.Character.CurrentVehicle.LightsOn
         * Player.Character.CurrentVehicle.MakeProofTo
         * Player.Character.CurrentVehicle.NeedsToBeHotwired
         * Player.Character.CurrentVehicle.PetrolTankHealth
         * Player.Character.CurrentVehicle.PlaceOnGroundProperly
         * Player.Character.CurrentVehicle.PlaceOnNextStreetProperly
         * Player.Character.CurrentVehicle.Position
         * Player.Character.CurrentVehicle.Repair
         * Player.Character.CurrentVehicle.Rotation
         * Player.Character.CurrentVehicle.SirenActive
         * Player.Character.CurrentVehicle.SoundHorn
         * Player.Character.CurrentVehicle.SpecularColor
         * Player.Character.CurrentVehicle.Speed
         * Player.Character.CurrentVehicle.Velocity
         * Player.Character.CurrentVehicle.Visible
         * Player.Character.CurrentVehicle.Wash
         */

        private string[] helpMessages = new string[] 
        { 
            "? - Shows help (page 1)",
            "? [integer:page] - Shows help by page",
            "allowemergencyservices [boolean] - Sets whether emergency services should be allowed or not",
            "applyforceplayer [float:x] [float:y] [float:z] - Applies force to you",
            "autosave - Autosaves the game",
            "clear - Clears all text from the DOS window",
            "die - Kills you",
            "displaytext [string] - Displays specified string as a message in the top left corner",
            "dropweapon - Drops your current weapon",
            "fps - Gets the current FPS",
            "freezeplayer [boolean] - Sets your player to be frozen or not",
            "gamemode - Gets the current gamemode (none = singleplayer)",
            "getarmor - Gets the amount of armor you have",
            "gethealth - Gets the amount of health you have",
            "getmoney - Gets the amount of money you have",
            "getposition - Gets the current position of the player",
            "gravity [boolean] - Sets whether there is gravity or not",
            "heightaboveground - Gets your height above the ground",
            "helmet [boolean] - Sets if you are wearing a helmet",
            "help - Shows help (page 1)",
            "help [integer:page] - Shows help for commands by page",
            "isinarea [float:pos1:x] [float:pos1:y] [float:pos1:z] [float:pos2:x] [float:pos2:y] [float:pos2:z]",
            "kill - Kills you",
            "maddrivers [boolean] - Sets if there should be mad drivers or not",
            "networkmode - Gets the current network mode",
            "nevergetstired [boolean] - Sets whether you never get tired or not",
            "playercount - Gets the number of players in the current server",
            "playerid - Gets the player's id",
            "playerinvincible [boolean] - Sets whether you are invincible or not",
            "playervisible [boolean] - Sets whether your player is visible or not",
            "ragdoll - Makes you ragdoll or stop ragdolling",
            "randomizeoutfit - Randomizes your outfit",
            "seatbelt [boolean] - Sets whether you have a seatbelt on or not",
            "setarmor [integer] - Sets the amount of armor you have",
            "sethealth [integer] - Sets the amount of health you have",
            "setmoney [integer] - Sets the amount of money you have",
            "setposition [float:x] [float:y] [float:z] - Sets your character's/your character's vehicle's position to the specified coordinates",
            "timescale [float] - Sets the timescale",
            "waypointposition - Gets the current waypoint's position, if there is one"
        };

        private void ProcessCommand(string commandAndParams)
        {
            commandAndParams = commandAndParams.ToLower();
            string[] commandAndParamsSeperated = commandAndParams.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
            string command = commandAndParamsSeperated[0];
            string[] parameters;
            if (commandAndParamsSeperated.Length > 1)
            {
                parameters = new string[commandAndParamsSeperated.Length - 1];
                for (int i = 1; i < commandAndParamsSeperated.Length; i++)
                {
                    parameters[i - 1] = commandAndParamsSeperated[i];
                }
            }
            else
            {
                parameters = new string[0];
            }

            switch (parameters.Length)
            {
                case 0:
                    {
                        switch (command)
                        {
                            case "help":
                            case "?":
                                {
                                    PrintHelp(1);
                                }
                                break;
                            case "autosave":
                                {
                                    Game.DoAutoSave();
                                    dosMessages.Add("Autosaved");
                                }
                                break;
                            case "fps":
                                {
                                    dosMessages.Add("FPS: " + Game.FPS.ToString());
                                }
                                break;
                            case "getposition":
                                {
                                    dosMessages.Add("Current player position: " + Player.Character.Position.ToString());
                                }
                                break;
                            case "waypointposition":
                                {
                                    if (Game.GetWaypoint() != null) dosMessages.Add("Current waypoint position: " + Game.GetWaypoint().Position.ToString());
                                }
                                break;
                            case "gamemode":
                                {
                                    dosMessages.Add("Gamemode: " + Game.MultiplayerGameMode.ToString());
                                }
                                break;
                            case "networkmode":
                                {
                                    dosMessages.Add("Network mode: " + Game.NetworkMode.ToString());
                                }
                                break;
                            case "playercount":
                                {
                                    dosMessages.Add("Player count: " + Game.PlayerCount.ToString());
                                }
                                break;
                            case "playerid":
                                {
                                    dosMessages.Add("Player ID: " + Player.ID);
                                }
                                break;
                            case "getarmor":
                                {
                                    dosMessages.Add("Armor: " + Player.Character.Armor.ToString());
                                }
                                break;
                            case "gethealth":
                                {
                                    dosMessages.Add("Health: " + Player.Character.Health.ToString());
                                }
                                break;
                            case "getmoney":
                                {
                                    dosMessages.Add("Money: " + Player.Money);
                                }
                                break;
                            case "die":
                            case "kill":
                                {
                                    Player.Character.Die();
                                    dosMessages.Add(GenerateDeathMessage());
                                }
                                break;
                            case "dropweapon":
                                {
                                    Player.Character.DropCurrentWeapon();
                                    dosMessages.Add("Dropped current weapon");
                                }
                                break;
                            case "ragdoll":
                                {
                                    dosMessages.Add(!Player.Character.isRagdoll ? "Ragdolling" : "Stopping ragdoll"); //keep backwards
                                    Player.Character.isRagdoll = !Player.Character.isRagdoll;
                                }
                                break;
                            case "clear":
                                {
                                    dosMessages.Clear();
                                }
                                break;
                            case "heightaboveground":
                                {
                                    dosMessages.Add("Height above ground: " + Player.Character.HeightAboveGround.ToString());
                                }
                                break;
                            case "randomizeoutfit":
                                {
                                    Player.Character.RandomizeOutfit();
                                    dosMessages.Add("Randomized outfit");
                                }
                                break;
                        }
                    }
                    break;
                case 1:
                    {
                        switch (command)
                        {
                            case "help":
                            case "?":
                                {
                                    int page;
                                    if (int.TryParse(parameters[0], out page))
                                    {
                                        PrintHelp(page);
                                    }
                                }
                                break;
                            case "allowemergencyservices":
                                {
                                    bool allow;
                                    if (bool.TryParse(parameters[0], out allow))
                                    {
                                        Game.AllowEmergencyServices = allow;
                                        dosMessages.Add("Emergency services are now " + (allow ? "" : "not ") + "allowed");
                                    }
                                }
                                break;
                            case "displaytext":
                                {
                                    Game.DisplayText(parameters[0], 2000);
                                }
                                break;
                            case "maddrivers":
                                {
                                    bool mad;
                                    if (bool.TryParse(parameters[0], out mad))
                                    {
                                        Game.MadDrivers = mad;
                                        dosMessages.Add("Mad drivers are now " + (mad ? "" : "not ") + "allowed");
                                    }
                                }
                                break;
                            case "timescale":
                                {
                                    float timescale;
                                    if (float.TryParse(parameters[0], out timescale))
                                    {
                                        Game.TimeScale = timescale;
                                        dosMessages.Add("Timescale is now " + timescale.ToString());
                                    }
                                }
                                break;
                            case "setarmor":
                                {
                                    int armor;
                                    if (int.TryParse(parameters[0], out armor))
                                    {
                                        Player.Character.Armor = armor;
                                        dosMessages.Add("Armor is now " + armor.ToString());
                                    }
                                }
                                break;
                            case "sethealth":
                                {
                                    int health;
                                    if (int.TryParse(parameters[0], out health))
                                    {
                                        Player.Character.Health = health;
                                        dosMessages.Add("Health is now " + health.ToString());
                                    }
                                }
                                break;
                            case "setmoney":
                                {
                                    int money;
                                    if (int.TryParse(parameters[0], out money))
                                    {
                                        Player.Money = money;
                                        dosMessages.Add("Money is now " + money.ToString());
                                    }
                                }
                                break;
                            case "nevergetstired":
                                {
                                    bool never;
                                    if (bool.TryParse(parameters[0], out never))
                                    {
                                        Player.NeverGetsTired = never;
                                        dosMessages.Add("You now " + (never ? "never " : "") + "get tired");
                                    }
                                }
                                break;
                            case "helmet":
                                {
                                    bool helmet;
                                    if (bool.TryParse(parameters[0], out helmet))
                                    {
                                        Player.Character.ForceHelmet(helmet);
                                        dosMessages.Add("You are now " + (helmet ? "" : "not ") + "wearing a helmet");
                                    }
                                }
                                break;
                            case "freezeplayer":
                                {
                                    bool freeze;
                                    if (bool.TryParse(parameters[0], out freeze))
                                    {
                                        Player.Character.FreezePosition = freeze;
                                        dosMessages.Add("You are now " + (freeze ? "" : "not ") + "frozen");
                                    }
                                }
                                break;
                            case "gravity":
                                {
                                    bool gravity;
                                    if (bool.TryParse(parameters[0], out gravity))
                                    {
                                        World.GravityEnabled = gravity;
                                        dosMessages.Add("Gravity is now " + (gravity ? "enabled" : "disabled"));
                                    }
                                }
                                break;
                            case "playerinvincible":
                                {
                                    bool invincible;
                                    if (bool.TryParse(parameters[0], out invincible))
                                    {
                                        Player.Character.Invincible = invincible;
                                        dosMessages.Add("You are now " + (invincible ? "" : "not ") + "invincible");
                                    }
                                }
                                break;
                            case "playervisible":
                                {
                                    bool visible;
                                    if (bool.TryParse(parameters[0], out visible))
                                    {
                                        Player.Character.Visible = visible;
                                        dosMessages.Add("You are now " + (visible ? "" : "in") + "visible");
                                    }
                                }
                                break;
                            case "seatbelt":
                                {
                                    bool seatbelt;
                                    if (bool.TryParse(parameters[0], out seatbelt))
                                    {
                                        Player.Character.WillFlyThroughWindscreen = seatbelt;
                                        dosMessages.Add("Seatbelt now " + (seatbelt ? "on" : "off") + ". Click it or ticket");
                                    }
                                }
                                break;
                        }
                    }
                    break;
                case 3:
                    {
                        switch (command)
                        {
                            case "setposition":
                                {
                                    Vector3 position = Vector3.Zero;
                                    if (float.TryParse(parameters[0], out position.X) && float.TryParse(parameters[1], out position.Y) && float.TryParse(parameters[2], out position.Z))
                                    {
                                        Player.Character.Position = position;
                                        dosMessages.Add("Position set to " + position.ToString());
                                    }
                                }
                                break;
                            case "applyforceplayer":
                                {
                                    Vector3 force = Vector3.Zero;
                                    if (float.TryParse(parameters[0], out force.X) && float.TryParse(parameters[1], out force.Y) && float.TryParse(parameters[2], out force.Z))
                                    {
                                        Player.Character.isRagdoll = true;
                                        Player.Character.ApplyForce(force);
                                        dosMessages.Add("Applied force to player");
                                    }
                                }
                                break;
                        }
                    }
                    break;
                case 6:
                    {
                        switch (command)
                        {
                            case "isinarea":
                                {
                                    Vector3 pos1 = Vector3.Zero, pos2 = Vector3.Zero;
                                    if (float.TryParse(parameters[0], out pos1.X) && float.TryParse(parameters[1], out pos1.Y) && float.TryParse(parameters[2], out pos1.Z) && float.TryParse(parameters[3], out pos2.X) && float.TryParse(parameters[4], out pos2.Y) && float.TryParse(parameters[5], out pos2.Z))
                                    {
                                        dosMessages.Add("You are " + (Player.Character.isInArea(pos1, pos2, false) ? "" : "not ") + "in the area between " + pos1.ToString() + " & " + pos2.ToString());
                                    }
                                }
                                break;
                        }
                    }
                    break;
            }


            GC.Collect();
        }

        private string GenerateDeathMessage()
        {
            switch (mainRand.Next(1, 10))
            {
                case 1:
                    return Player.Name + " died of suicide today at " + string.Format("{0:hh:mm:ss tt}", DateTime.Now) + ". Good riddance.";
                case 2:
                    return "I told you I was sick. -" + Player.Name;
                case 3:
                    return "He always said his feet were killing him, nobody believed him.";
                case 4:
                    return "Ma loves Pa - Pa loves women. Ma caught Pa, with 2 in swimmin. Here lies Pa...";
                case 5:
                    return "Born " + yearRand.Next(1960, DateTime.Now.Year) + " - Died " + DateTime.Now.Year + ". Looked up the elevator shaft to see if the car was on the way down. It was.";
                case 6:
                    return "Here lies " + Player.Name + ". Age " + ageRand.Next(75, 108) + ". The good die young.";
                case 7:
                    return "Here lies an Atheist. All dressed up, and no place to go.";
                case 8:
                    return "First a cough that carried me off, then in a coffin they carried me off in.";
                default:
                    return "He loved bacon.";
            }

        }

        private void PrintHelp(int page)
        {
            int beginningNum = (page - 1) * 11;
            int endingNum = (page * 11 > helpMessages.Length ? helpMessages.Length : page * 11);
            for (int i = beginningNum; i < endingNum; i++)
            {
                dosMessages.Add(helpMessages[i]);
            }
            dosMessages.Add("Page " + page.ToString() + " out of " + Math.Ceiling((double)helpMessages.Length / 11).ToString());
        }

        private float MeasureStringWidth(string s, System.Drawing.Font font)
        {
            SizeF result;
            using (var image = new Bitmap(1, 1))
            {
                using (var g = System.Drawing.Graphics.FromImage(image))
                {
                    result = g.MeasureString(s, font);
                }
            }

            return result.Width;
        }

        private void Subtitle(string text, int time = 2000)
        {
            GTA.Native.Function.Call("PRINT_STRING_WITH_LITERAL_STRING_NOW", "STRING", text, time, 1);
        }



        //archives
        /*void KeyHoldingTimer_Tick(object sender, EventArgs e)
{
    if (keyBeingHeld)
    {
        bool atLeastOne = false;
        for (int iKey = 65; iKey < 90; iKey++)
        {
            if (isKeyPressed((Keys)iKey))
            {
                dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? ((Keys)iKey).ToString() : ((Keys)iKey).ToString().ToLower();
                atLeastOne = true;
            }
        }
        if (isKeyPressed(Keys.Space))
        {
            dosCurrentCommand += " ";
            atLeastOne = true;
        }
        else if (isKeyPressed(Keys.D0) || isKeyPressed(Keys.NumPad0))
        {
            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? ")" : "0";
            atLeastOne = true;
        }
        else if (isKeyPressed(Keys.D1) || isKeyPressed(Keys.NumPad1))
        {
            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "!" : "1";
            atLeastOne = true;
        }
        else if (isKeyPressed(Keys.D2) || isKeyPressed(Keys.NumPad2))
        {
            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "@" : "2";
            atLeastOne = true;
        }
        else if (isKeyPressed(Keys.D3) || isKeyPressed(Keys.NumPad3))
        {
            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "#" : "3";
            atLeastOne = true;
        }
        else if (isKeyPressed(Keys.D4) || isKeyPressed(Keys.NumPad4))
        {
            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "$" : "4";
            atLeastOne = true;
        }
        else if (isKeyPressed(Keys.D5) || isKeyPressed(Keys.NumPad5))
        {
            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "%" : "5";
            atLeastOne = true;
        }
        else if (isKeyPressed(Keys.D6) || isKeyPressed(Keys.NumPad6))
        {
            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "^" : "6";
            atLeastOne = true;
        }
        else if (isKeyPressed(Keys.D7) || isKeyPressed(Keys.NumPad7))
        {
            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "&" : "7";
            atLeastOne = true;
        }
        else if (isKeyPressed(Keys.D8) || isKeyPressed(Keys.NumPad8))
        {
            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "*" : "8";
            atLeastOne = true;
        }
        else if (isKeyPressed(Keys.D9) || isKeyPressed(Keys.NumPad9))
        {
            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "(" : "9";
            atLeastOne = true;
        }
        else if (isKeyPressed(Keys.OemPeriod))
        {
            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? ">" : ".";
            atLeastOne = true;
        }
        else if (isKeyPressed(Keys.OemOpenBrackets))
        {
            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "{" : "[";
            atLeastOne = true;
        }
        else if (isKeyPressed(Keys.OemCloseBrackets))
        {
            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "}" : "]";
            atLeastOne = true;
        }
        else if (isKeyPressed(Keys.Oemcomma))
        {
            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "<" : ",";
            atLeastOne = true;
        }
        else if (isKeyPressed(Keys.OemQuestion))
        {
            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "?" : "/";
            atLeastOne = true;
        }
        else if (isKeyPressed(Keys.OemBackslash))
        {
            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "|" : @"\";
            atLeastOne = true;
        }
        else if (isKeyPressed(Keys.OemSemicolon))
        {
            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? ":" : ";";
            atLeastOne = true;
        }
        else if (isKeyPressed(Keys.OemQuotes))
        {
            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "\"" : "'"; // \" == "
            atLeastOne = true;
        }
        else if (isKeyPressed(Keys.Oemplus))
        {
            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "+" : "=";
            atLeastOne = true;
        }
        else if (isKeyPressed(Keys.OemMinus))
        {
            dosCurrentCommand += isKeyPressed(Keys.ShiftKey) ? "_" : "-";
            atLeastOne = true;
        }
        keyBeingHeld = atLeastOne;
        Subtitle("atLeastOne = " + atLeastOne.ToString());
    }
}*/
    }
}
