This is a (slight) recreation of DOS, but instead of having DOS commands, it has commands for controlling your GTA IV!
This mod is still in beta. It will get many more commands and customization features in coming updates.

Full list of commands:
? - Shows help (page 1),
? [integer:page] - Shows help by page,
allowemergencyservices [boolean] - Sets whether emergency services should be allowed or not,
applyforceplayer [float:x] [float:y] [float:z] - Applies force to you,
autosave - Autosaves the game,
clear - Clears all text from the DOS window,
die - Kills you,
displaytext [string] - Displays specified string as a message in the top left corner,
dropweapon - Drops your current weapon,
fps - Gets the current FPS,
freezeplayer [boolean] - Sets your player to be frozen or not,
gamemode - Gets the current gamemode (none = singleplayer),
getarmor - Gets the amount of armor you have,
gethealth - Gets the amount of health you have,
getmoney - Gets the amount of money you have,
getposition - Gets the current position of the player,
gravity [boolean] - Sets whether there is gravity or not,
heightaboveground - Gets your height above the ground,
helmet [boolean] - Sets if you are wearing a helmet,
help - Shows help (page 1),
help [integer:page] - Shows help for commands by page,
isinarea [float:pos1:x] [float:pos1:y] [float:pos1:z] [float:pos2:x] [float:pos2:y] [float:pos2:z],
kill - Kills you,
maddrivers [boolean] - Sets if there should be mad drivers or not,
networkmode - Gets the current network mode,
nevergetstired [boolean] - Sets whether you never get tired or not,
playercount - Gets the number of players in the current server,
playerid - Gets the player's id,
playerinvincible [boolean] - Sets whether you are invincible or not,
playervisible [boolean] - Sets whether your player is visible or not,
ragdoll - Makes you ragdoll or stop ragdolling,
randomizeoutfit - Randomizes your outfit,
seatbelt [boolean] - Sets whether you have a seatbelt on or not,
setarmor [integer] - Sets the amount of armor you have,
sethealth [integer] - Sets the amount of health you have,
setmoney [integer] - Sets the amount of money you have,
setposition [float:x] [float:y] [float:z] - Sets your character's/your character's vehicle's position to the specified coordinates,
timescale [float] - Sets the timescale,
waypointposition - Gets the current waypoint's position, if there is one

How to install:
Install an ASI Loader (YAASIL recommended) and .NET Scripthook.
Copy DOSIV.net.dll and DOSIV.ini to the scripts folder in your GTA IV/EFLC directory.

Controls:
Right-CTRL + C to open/close the DOS window. Changeable in the INI file.
All of the alphabetical and numerical keys for entering in commands.
Backspace to remove the last letter in the current command.
Enter to enter commands in.

How to use:
Press Right-CTRL + C to open the DOS window, type a command like "help" in, then press Enter to enter the command in.

How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with the cs file in this archive
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
